@extends('layouts.app')

@section('content')

    <div class="row col-md-8 col-md-offset-2">

            <h3 align="center">All Clients</h3>
            <table class="table table-striped table-responsive table-hover">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Gender</th>
                    <th>Email</th>
                    <th>Kids</th>
                    <th></th>
                </tr>

                @foreach($clients as $item)
                    <tr>
                        <td>{{$item->fname}}</td>
                        <td>{{$item->lname}}</td>
                        <td>{{$item->gender}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->boys + $item->girls}}</td>
                        <td>
                            <a href="{{url('/view-client/' . $item->cid)}}" class="btn btn-primary">View</a>
                        </td>
                    </tr>
                @endforeach

            </table>

    </div>


@endsection