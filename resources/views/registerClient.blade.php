@extends('layouts.app')

@section('content')



     <div class="container">

         @if( Session::has('error') )
             <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
         @endif

         @if( Session::has('success') )
             <div class=" alert alert-success" align="center">{{Session::get('success')}}</div>
         @endif

         <img class="main" src="{{url('/images/logo.png')}}">
         <div class="panel panel-default">
             <div class="panel-heading">
                 Register a new Client
             </div>
             <div class="panel-body">
                 <form class="form-group" action="{{url('/register-client')}}" method="post">

                     {{csrf_field()}}
                      <div >

                          <div class="col-md-4 form-group form-inline">
                              <label for="fname" class=" control-label">Firstname:</label> <br>
                              <input placeholder="firstname" id="fname" type="text" class="form-control" name="fname" value="{{ old('fname') }}" >
                          </div>

                          <div class="col-md-4 form-inline form-group">
                              <label for="mname" class=" control-label">Middlename:</label> <br>
                              <input placeholder="Middlename" id="mname" type="text" class="form-control" name="mname" value="{{ old('fname') }}" >
                          </div>

                          <div class="col-md-4 form-inline form-group">
                              <label for="sname" class=" control-label">Surname:</label> <br>
                              <input placeholder="Surname" id="sname" type="text" class="form-control" name="lname" value="{{ old('fname') }}" >
                          </div>

                      </div>


                      <div class="form-group">
                              <label for="dob" class="col-md-4 control-label">Date Of Birth</label> <br>
                              <input id="dob" type="date" placeholder="12-01-1990" class="form-control" name="dob" value="{{ old('dob') }}" >
                      </div>

                     <div class="form-group">
                         <label  class="col-md-4 control-label">Postal Address</label> <br>
                         <textarea class="form-control" name="paddress">{{old('paddress')}}</textarea>
                     </div>

                     <div class="form-group">
                         <label  class="col-md-4 control-label">House Address</label> <br>
                         <textarea class="form-control" name="haddress">{{old('haddress')}}</textarea>
                     </div>

                     <div class="form-group">
                         <label class="col-md-4 control-label">Phone</label> <br>
                         <input  type="text" placeholder="+233231234567" class="form-control" name="phone" value="{{ old('phone') }}" >
                     </div>

                     <div class="form-group">
                         <label  class="col-md-4 control-label">Religion</label> <br>
                         <input  type="text"  class="form-control" name="religion" value="{{ old('religion') }}" >
                     </div>

                     <div class="form-group">
                         <label class="col-md-4 control-label">Hometown</label> <br>
                         <textarea class="form-control" name="hometown">{{old('hometown')}}</textarea>
                     </div>

                     <div class="form-group">
                         <select name="maritalStatus" class="form-control">
                             <option>Married</option>
                             <option>Single</option>
                             <option>Divorced</option>
                             <option>Separated</option>
                         </select>
                     </div>

                     <div class="form-group">
                         <label  class="col-md-4 control-label">Number of Male children</label> <br>
                         <input  type="number"  class="form-control" name="boys" value="{{ old('boys') }}" >
                     </div>

                     <div class="form-group">
                         <label  class="col-md-4 control-label">Number of Female children</label> <br>
                         <input  type="number" class="form-control" name="girls" value="{{ old('girls') }}" >
                     </div>

                     <div class="form-group">
                         <label  class="col-md-4 control-label">Gender</label> <br>
                         <select name="gender">
                             <option>Male</option>
                             <option>Female</option>
                         </select>
                     </div>

                     <div class="form-group">
                         <label  class="col-md-4 control-label">Email</label> <br>
                         <input  type="email"  class="form-control" name="email" value="{{ old('email') }}" >
                     </div>

                     <div class="form-group">
                         <label  class="col-md-4 control-label">Method</label> <br>
                         <select name="method">
                             @foreach($male as $item)
                                 <option>{{$item->title}}</option>
                             @endforeach

                             @foreach($female as $item)
                                 <option>{{$item->title}}</option>
                             @endforeach
                         </select>

                     </div>

                     <button class="btn btn-primary">Register Client</button>
                     <button type="reset" class="btn btn-warning">Clear</button>


                 </form>
             </div>
         </div>
     </div>

@endsection