@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-md-3 green well well-sm">
            {{count($male)}} Male Procedures
        </div>

        <div class="col-md-3 green well well-sm">
            {{count($female)}} Female Procedures
        </div>

        <div class="col-md-3 green well well-sm">
            {{count($faq)}} FAQ
        </div>

        <div class="col-md-3 green well well-sm">
            {{$total}} Clients
        </div>
    </div>

    <div class="row">
        <h3>Recent Clients</h3>
        <table class="table table-striped table-responsive table-hover">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Gender</th>
                <th>Email</th>
                <th>Kids</th>
                <th></th>
            </tr>

            @foreach($clients as $item)
            <tr>
                <td>{{$item->fname}}</td>
                <td>{{$item->lname}}</td>
                <td>{{$item->gender}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->boys + $item->girls}}</td>
                <td>
                    <a href="{{url('/view-client/' . $item->cid)}}" class="btn btn-primary">View</a>
                </td>
            </tr>
            @endforeach

        </table>
    </div>


    {{--<div class="row col-md-8 col-md-offset-2">--}}
        {{--<h3>Refer client</h3>--}}

        {{--<div class="form-group">--}}
            {{--<select>--}}
                {{--@foreach($clients as $item)--}}
                {{--<option>{{$item->fname}} {{$item->lname}}</option>--}}
                {{--@endforeach--}}
            {{--</select>--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
        {{--<label >Reason:</label>--}}
        {{--<textarea class="form-control" name="reason"></textarea>--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--<label >Destination:</label>--}}
            {{--<textarea class="form-control" name="destination"></textarea>--}}
        {{--</div>--}}
        {{--<a class="btn btn-primary">Refer</a>--}}

    {{--</div>--}}



</div>

@endsection