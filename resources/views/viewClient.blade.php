@extends('layouts.app')

@section('content')



    <div class="container">

        @if( Session::has('error') )
            <div class="alert alert-danger" align="center">{{Session::get('error')}}</div>
        @endif

        @if( Session::has('success') )
            <div class=" alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif

        <img class="main" src="{{url('/images/logo.png')}}">
        <div class="panel panel-default">
            <div class="panel-heading">
                Client - {{$client->fname}} {{$client->mname}} {{$client->oname}}
            </div>
            <div class="panel-body">
                <form class="form-group" method="post">


                    <div class="form-group">
                        <label for="dob" class="col-md-4 control-label">Date Of Birth</label> <br>
                        <input id="dob" type="date" pattern="[0-9]{1,2}-[0-9]{1,2}-[0-9]{2,4}"  placeholder="12-01-1990" class="form-control" name="dob" disabled value="{{ $client->dob }}" >
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">Postal Address</label> <br>
                        <textarea class="form-control" name="paddress" disabled>{{$client->paddress}}</textarea>
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">House Address</label> <br>
                        <textarea disabled class="form-control" name="haddress">{{$client->haddress}}</textarea>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone</label> <br>
                        <input  type="text" pattern="[+][0-9+]" placeholder="+233231234567" class="form-control" name="phone" disabled value="{{ $client->phone }}" >
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">Religion</label> <br>
                        <input  type="text"  class="form-control" name="religion" disabled value="{{ $client->religion }}" >
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Hometown</label> <br>
                        <textarea class="form-control" name="hometown" disabled>{{$client->hometown}}</textarea>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Marital Status</label> <br>
                        <input type="text" value="{{$client->maritalStatus}}" disabled>
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">Number of Male children</label> <br>
                        <input disabled  type="number"  class="form-control" name="boys" value="{{ $client->boys }}" >
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">Number of Female children</label> <br>
                        <input  type="number" class="form-control" name="girls" disabled value="{{ $client->girls }}" >
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">Gender</label> <br>
                        <input type="text" value="{{$client->gender}}" disabled>
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">Email</label> <br>
                        <input  type="email"  class="form-control" disabled name="email" value="{{ $client->email }}" >
                    </div>

                    <div class="form-group">
                        <label  class="col-md-4 control-label">Method</label> <br>
                        <input type="text" value="{{$client->method}}" disabled>
                    </div>

                    <a onclick="window.history.back()" class="btn btn-primary">Go back</a>

                </form>
            </div>
        </div>
    </div>

@endsection