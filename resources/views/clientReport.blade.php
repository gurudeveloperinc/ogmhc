<html>
<head>

    <link href="{{url('css/app.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
<div align="center" class="col-md-12 main " >
    <div>
        <img src="{{url('/images/logo.png')}}" style="height: 100px;" class="logo">
        <h3 class="logoHeader" style="font-size: 30px;color:#8B8E45">OGMHC FAMILY PLANNING UNIT</h3>
    </div>

    <style>
        body{
            background-color: white;
        }
        td,th{
            text-align: center;
        }

    </style>

    <h3 style="color:#8B8E45">CLIENT REPORT</h3>
    <h3>Generated on: {{\Carbon\Carbon::now()->toDateTimeString()}}</h3>

    <table class="table table-striped table-responsive table-hover">
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>DOB</th>
            <th>Phone</th>
            <th>Religion</th>
            <th>Hometown</th>
            <th>Marital Status</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Method</th>
            <th>House Address</th>
            <th>Postal Address</th>
            <th>Male Kids</th>
            <th>Female Kids</th>

        </tr>

        @foreach($clients as $item)
            <tr>
                <td>{{$item->fname}}</td>
                <td>{{$item->lname}}</td>
                <td>{{$item->dob}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->religion}}</td>
                <td>{{$item->hometown}}</td>
                <td>{{$item->maritalStatus}}</td>
                <td>{{$item->gender}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->method}}</td>
                <td>{{$item->houseAddress}}</td>
                <td>{{$item->postalAddress}}</td>
                <td>{{$item->boys}}</td>
                <td>{{$item->girls}}</td>
            </tr>
        @endforeach

    </table>

</div>
</body>
</html>