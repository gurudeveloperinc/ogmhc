<!DOCTYPE html>
<html>
<head>
    <title>Osu Government Maternity Home Clinic </title>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Custom Theme files -->
    <!--theme-style-->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!--//theme-style-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Scientist Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</head>
<body>
<!--header-->
<div class="header">
    <div class="container">
        <div class="logo">
            <h1><a href="{{url('/')}}">
                    OGMHC <br>
                    <span> FAMILY PLANNING UNIT</span>
                </a></h1>
        </div>
        <div class="top-nav">
            <span class="menu"><img src="images/menu.png" alt=""> </span>
            <ul>
                <li class="active"><a href="index.html">Home</a></li>
                <li><a href="#services" class="hvr-sweep-to-bottom">Services</a></li>
                <li><a href="#contact" class="hvr-sweep-to-bottom">Contact</a></li>
                <li><a href="{{url('/login')}}" class="hvr-sweep-to-bottom">Login</a></li>
            </ul>
            <div class="clearfix"> </div>
            <!--script-->
            <script>
                $("span.menu").click(function(){
                    $(".top-nav ul").slideToggle(500, function(){
                    });
                });
            </script>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!---->
</div>
<div class="content">
    <div class="container">
        <!--content-top-->
        <div class="content-top">
            <div class="content-top1">
                <div class=" col-md-4 grid-top">
                    <div class="top-grid">
                        <i class="glyphicon glyphicon-book"></i>
                        <div class="caption">
                            <h3>VISION STATEMENT</h3>
                            <p> A specialized clinic offering integrated services in the prevention and management of maternal related diseases</p>
                        </div>
                    </div>
                </div>
                <div class=" col-md-4 grid-top">
                    <div class="top-grid top">
                        <i class="glyphicon glyphicon-time home1 "></i>
                        <div class="caption">
                            <h3>MISSION STATEMEMT</h3>
                            <p>To work in collaboration with all partners in health to ensure that every mother in Osu Klottey community and its environs are adequately informed about maternal and child health</p>
                        </div>
                    </div>
                </div>
                <div class=" col-md-4 grid-top">
                    <div class="top-grid">
                        <i class="glyphicon glyphicon-edit "></i>
                        <div class="caption">
                            <h3>OUR DEPARTMEMTS</h3>
                            <p> Antenatal</p>
                            <p> Deliveries</p>
                            <p> Postnatal</p>
                            <p> Child Welfare clinic</p>
                            <p> Family Planning Unit</P>
                            <p> STI's/Adolescent Health</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!--//content-top-->
        <!--cpntent-mid-->
        <div class="content-middle">
            <div class="col-md-6 content-mid">
                <a href="single.html"><img class="img-responsive" src="{{url('images/bcm.jpg')}}" alt=""></a>
            </div>
            <div class="col-md-5 content-mid1">
                <i class="icon-circle-question-mark"> </i>
                <h2>METHODS OF CONTRACEPTION AVAILABLE</h2>
               <p>There are different methods of contraception, including:<br>

                   long-acting reversible contraception, such as an implant, or an intra uterine device
                   hormonal contraception such as contraceptive pills – “the pill”,  the injection and vaginal rings
                   barriers methods, such as condoms and diaphragms
                   fertility awareness
                   emergency contraception
                   permanent contraception, such as vasectomy and tubal ligation.</p>

            </div>

            <div class="clearfix"> </div>

        </div>
        <!--//content-mid-->
        <!--content-left-->
        <div class="content-left">
            <div class="col-md-4 content-left-top">
                <a href="single.html"><img class="img-responsive" src="{{url('images/fp4.jpg')}}" alt=""></a>
                <div class=" content-left-bottom">
                    <h4><i class="glyphicon glyphicon-ok"></i><a href="#">Six Months Old Baby Undergoing Weighing</a></h4>
                    <p>A baby, who was born 24 weeks premature and weighed just 500gm at the time of birth, has managed to fight all odds and survive.
                        Doctors at Osu ...</p>
                    <a href="/" class="hvr-icon-wobble-horizontal">Read More</a>
                </div>
            </div>
            <div class="col-md-4 content-left-top">
                <a href="single.html"><img class="img-responsive" src="{{url('images/fp7.jpg')}}" alt=""></a>
                <div class=" content-left-bottom">
                    <h4><i class="glyphicon glyphicon-ok"></i><a href="#">Mid Wife Assisting Nursing Mother Breast feed her baby</a></h4>
                    <p>A new mom's breastfeeding experience can be as unique as her baby. Some take to it like they've been doing it forever, while others struggle through pain, infections and supply issues....</p>
                    <a href="/" class="hvr-icon-wobble-horizontal">Read More</a>
                </div>
            </div>
            <div class="col-md-4 content-left-top">
                <a href="single.html"><img class="img-responsive" src="{{url('images/fp8.jpg')}}" alt=""></a>
                <div class=" content-left-bottom">
                    <h4><i class="glyphicon glyphicon-ok"></i><a href="">Vaccines to protect babies from seven killer diseases</a></h4>
                    <p>If you follow the OGMHC's recommended vaccination schedule, your child will receive 49 doses of 14 vaccines by the time he/she is 6 years of age. And by the age of 18, the unit recommends...</p>
                    <a href="/" class="hvr-icon-wobble-horizontal">Read More</a>
                </div>
            </div>

            <div class="clearfix"> </div>
        </div>
        <!--//content-left-->
    </div>
    <!--content-right-->
    <div class="content-right">
        <div class="col-md-6 content-right-top">
            <h3>Men have five birth control options:</h3>
            <ul>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Abstinence</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Condoms</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Outercourse</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Vasectomy</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Withdrawal</a></li>

            </ul>
        </div>
        <div class="col-md-6 content-right-top col1">
            <h3>Birth Control options for women include:</h3>
            <ul>

                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>  Abstinence</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Birth Control Sponge</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Birth Control Patch</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Birth Control Vaginal Ring </a></li>
                <li><a href="#"><i class="glyphicon glyphicon-ok"> </i>Birth Control Implant</a></li>


            </ul>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!--//content-right-->
    <!--content-bottom-->
    <!--
    <div class="content-bottom">
        <div class="container">
            <h4>Events</h4>
            <div class="col-md-6 content-bottom-top">
                <span>30<small>Jan</small></span>
                <p>Our goal at OGMHC is to give you up-to-date, clear information that helps you better understand your birth control options. We hope these pages give you the facts and tools you need</p>
                <a href="single.html" class="hvr-icon-wobble-horizontal">Read More</a>
            </div>
            <div class="col-md-6 content-bottom-top">
                <span>15<small>mar</small></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                <a href="single.html" class="hvr-icon-wobble-horizontal">Read More</a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!--//content-bottom-->
    <!--address-->
 .
    <div class="address">
        <div class="container">
            <div class=" address-more">
                <h3>Address</h3>
                <div class="col-md-4 address-grid">
                    <i class="glyphicon glyphicon-map-marker"></i>
                    <div class="address1">
                        <p>No 32 Salem Road, CDC Link</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-4 address-grid ">
                    <i class="glyphicon glyphicon-phone"></i>
                    <div class="address1">
                        <p>+233208441082</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-4 address-grid ">
                    <i class="glyphicon glyphicon-envelope"></i>
                    <div class="address1">
                        <p><a href="mailto:info@ogmhc.com"> info@ogmhc.com</a></p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!--//address-->
</div>
<!--footer-->
<div class="footer">
    <div class="container">
        <div class="col-md-4 footer-top">
            <h3><a href="index.html">OGMHC</a></h3>
        </div>
        <div class="col-md-4 footer-top1">
            <ul class="social">
                <li><a href="#"><i> </i></a></li>
                <li><a href="#"><i class="dribble"> </i></a></li>
                <li><a href="#"><i class="facebook"> </i></a></li>
            </ul>
        </div>
        <div class="col-md-4 footer-top2">
            <p >© 2017 OGMHC. All rights reserved</p>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
</body>
</html>