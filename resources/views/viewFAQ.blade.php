@extends('layouts.app')

@section('content')

    <div class="row col-md-8 col-md-offset-2">
        @if( Session::has('success') )
            <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif


        @if( Session::has('error') )
            <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
        @endif

        <h3 align="center">All FAQ</h3>
        <table class="table table-striped  table-responsive table-hover">
            <tr>
                <th>Question</th>
                <th>Answer</th>
                <th></th>
            </tr>

            @foreach($faq as $item)
                <tr>
                    <td>{{$item->question}}</td>
                    <td>{{$item->answer}}</td>
                    <td>
                        <a href="{{url('/delete-faq/' . $item->fid)}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>


@endsection