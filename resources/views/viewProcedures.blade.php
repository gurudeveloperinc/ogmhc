@extends('layouts.app')

@section('content')
    <?php use Illuminate\Support\Facades\Input; ?>
    <div class="row col-md-8 col-md-offset-2 ">
        @if( Session::has('success') )
            <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif


        @if( Session::has('error') )
            <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
        @endif

        <h3 align="center">
            @if(empty(Input::get('gender')) || Input::get('gender') == "MALE")
                MALE
                @else
                FEMALE
            @endif

        </h3>

        <form action="{{url('/view-procedures')}}">
            <div class="form-group">
                <label style="width:100px;">Gender</label>
                <select name="gender">
                    <option>MALE</option>
                    <option>FEMALE</option>
                </select>
                <button type="submit" class="btn btn-primary">Find</button>
            </div>

        </form>


        @if(empty(Input::get('gender')) || Input::get('gender') == "MALE")

        <table class="table table-striped  table-responsive table-hover">
            <tr>
                <th>Type</th>
                <th>Title</th>
                <th>Description</th>
                <th></th>
            </tr>

            @foreach($male as $item)
                <tr>
                    <td>{{$item->type}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{$item->description}}</td>
                    <td>
                        <a href="{{url('/delete-male-procedure/' . $item->pid)}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach

        </table>

        @endif

        @if(\Illuminate\Support\Facades\Input::get('gender') == "FEMALE")

            <table class="table table-striped  table-responsive table-hover">
                <tr>
                    <th>Type</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th></th>
                </tr>

                @foreach($female as $item)
                    <tr>
                        <td>{{$item->type}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->description}}</td>
                        <td>
                            <a href="{{url('/delete-female-procedure/' . $item->pid)}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach

            </table>

        @endif




    </div>



@endsection