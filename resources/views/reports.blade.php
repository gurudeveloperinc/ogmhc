@extends('layouts.app')

@section('content')

    <div class="container">

        <div align="center">

            <h3>Download Reports</h3> <br>

            <a href="{{url('female-procedure-report')}}" class="btn btn-primary">Download Female Procedure Report</a>
            <a href="{{url('male-procedure-report')}}" class="btn btn-primary">Download Male Procedure Report</a>
            <a href="{{url('client-report')}}" class="btn btn-primary">Download Client Report</a>
        </div>
    </div>

@endsection