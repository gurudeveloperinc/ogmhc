<html>
<head>

    <link href="{{url('css/app.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
<div align="center" class="col-md-12 main " >
    <div>
        <img src="{{url('/images/logo.png')}}" style="height: 100px;" class="logo">
        <h3 class="logoHeader" style="font-size: 30px;color:#8B8E45">OGMHC FAMILY PLANNING UNIT</h3>
    </div>

    <style>
        body{
            background-color: white;
        }
        td,th{
            text-align: center;
        }

    </style>


    <h3 style="color:#8B8E45">FEMALE PROCEDURE REPORT</h3>
    <h3>Generated on: {{\Carbon\Carbon::now()->toDateTimeString()}}</h3>

    <table class="table table-striped table-responsive table-hover">
        <tr>
            <th>Type</th>
            <th>Title</th>
            <th>Description</th>
            <th>Added On</th>
        </tr>

        @foreach($female as $item)
            <tr>
                <td>{{$item->type}}</td>
                <td>{{$item->title}}</td>
                <td>{{$item->description}}</td>
                <td>{{$item->created_at}}</td>
            </tr>
        @endforeach

    </table>

</div>
</body>
</html>