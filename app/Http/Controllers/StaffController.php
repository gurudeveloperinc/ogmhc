<?php

namespace App\Http\Controllers;

use App\client;
use App\faq;
use App\female;
use App\male;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StaffController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function dashboard() {

		$faq = faq::all()->sortByDesc("created_at");
		$male = male::all()->sortByDesc("created_at");
		$female = female::all()->sortByDesc("created_at");
		$clients = client::all()->take(3)->sortByDesc("created_at");
		$total = count(client::all()->sortByDesc("created_at"));
		return view('dashboard',[
			'clients' => $clients,
			'faq' => $faq,
			'male' => $male,
			'female' => $female,
			'total' => $total
		]);
	}


	public function viewProcedures() {


		$male = male::all()->sortByDesc("created_at");
		$female = female::all()->sortByDesc("created_at");

		return view('viewProcedures',[
			'male'=> $male,
			'female' => $female
		]);
	}

	public function viewRefferals() {
		return view('viewRefferals');
	}



	public function viewClients() {
		$clients = client::all()->sortByDesc("created_at");
		return view('viewClients',[
			'clients' => $clients
		]);
	}

	public function registerClient() {
		$male = male::all();
		$female = female::all();
		return view('registerClient',[
			'male' => $male,
			'female' => $female
		]);
	}

	public function viewFAQ(){
		$faq = faq::all()->sortByDesc("created_at");
		return view('viewFAQ',[
			'faq' => $faq
		]);
	}

	public function addFAQ() {
		return view('addFAQ');
	}

	public function addRefferals() {
		return view('addRefferals');
	}

	public function addProcedure() {
		return view('addProcedure');
	}

	public function deleteMaleProcedure( Request $request, $pid ) {
		male::destroy($pid);

		$request->session()->flash("success","Procedure Deleted");

		return redirect('/view-procedures');
	}

	public function deleteFemaleProcedure( Request $request, $pid ) {
		female::destroy($pid);

		$request->session()->flash("success","Procedure Deleted");

		return redirect('/view-procedures');
	}

	public function deleteFAQ( Request $request, $fid ) {
		faq::destroy($fid);

		$request->session()->flash("success", "FAQ Deleted");

		return redirect('/view-faq');
	}

	public function viewClient( $cid ) {
		$client = client::find($cid);
		return view('viewClient',[
			'client' => $client
		]);
	}

	public function postRegisterClient(Request $request) {

		$fname = $request->input('fname');
		$mname = $request->input('mname');
		$lname = $request->input('lname');

		$dob = $request->input('dob');
		$paddress = $request->input('paddress');
		$haddress = $request->input('haddress');
		$phone = $request->input('phone');
		$religion = $request->input('religion');
		$hometown = $request->input('hometown');
		$maritalStatus = $request->input('maritalStatus');
		$boys = $request->input('boys');
		$girls = $request->input('girls');
		$gender = $request->input('gender');
		$email = $request->input('email');
		$method = $request->input('method');
		$password = str_random(5);
		$client = new client();
		$client->fname = $fname;
		$client->mname = $mname;
		$client->lname = $lname;
		$client->dob = $dob;
		$client->paddress = $paddress;
		$client->haddress = $haddress;
		$client->phone = $phone;
		$client->religion = $religion;
		$client->hometown = $hometown;
		$client->maritalStatus = $maritalStatus;
		$client->boys = $boys;
		$client->girls = $girls;
		$client->gender = $gender;
		$client->email = $email;
		$client->method = $method;
		$client->sid = Auth::user()->sid;
		$client->password = $password;
		$status = $client->save();

		mail($email,"Your OGHMC credentials", "Thank you for joining OGHMC. Please login to the app with your email and this password: $password");

		if ($status) $request->session()->flash("success", "Client registered successfully");

		return view('registerClient');
	}

	public function postAddRefferal( Request $request ) {
		return view('addRefferal');
	}

	public function postAddProcedure( Request $request ) {

		if($request->hasFile("image")){
			$filename = $request->file("image")->getClientOriginalName();
			$request->file("image")->move("uploads/",$filename);
			$image = url('/uploads/'. $filename);
		} else{
			$image = url('/uploads/default.jpg');
		}

		$gender = $request->input('gender');

		if($gender == "MALE"){
			$male = new male();
			$male->type = $request->input('type');
			$male->title = $request->input('title');
			$male->description = $request->input('description');
			$male->image = $image;
			$male->video = $request->input('video');
			$male->sid = Auth::user()->sid;
			$status = $male->save();

		}
		else if($gender == "FEMALE"){
			$female = new female();
			$female->type = $request->input('type');
			$female->title = $request->input('title');
			$female->description = $request->input('description');
			$female->image = $image;
			$female->video = $request->input('video');
			$female->sid = Auth::user()->sid;
			$status = $female->save();
		}

		if($status) $request->session()->flash("success", "Successfully added procedure");
		else $request->session()->flash("error","Sorry an error occurred.");

		return view('addProcedure');
	}

	public function postAddFAQ( Request $request ) {

		$faq = new faq();
		$faq->question = $request->input('question');
		$faq->answer = $request->input('answer');
		$faq->sid = Auth::user()->sid;
		$status = $faq->save();

		if($status) $request->session()->flash("success", "Successfully added FAQ");
		else $request->session()->flash("error","Sorry an error occurred.");

		return view('addFAQ');
	}

	public function reports() {
		return view('reports');
	}

	public function clientReport() {

		$clients = client::all();

		return view('clientReport',[
			'clients' => $clients
		]);

	}

	public function maleProcedureReport() {

		$male = male::all();

		return view('maleProcedureReport',[
			'male' => $male
		]);
	}

	public function femaleProcedureReport() {

		$female = female::all();

		return view('femaleProcedureReport',[
			'female' => $female
		]);
	}

	public function femaleProcedurePDF() {

		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->femaleProcedureReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'portrait' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream("OGMHC_Female_Procedure_Report_" . Carbon::now()->toDateString());
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}


	}

	public function maleProcedurePDF() {

		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->maleProcedureReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'portrait' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream("OGMHC_Male_Procedure_Report_" . Carbon::now()->toDateString());
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}


	}


	public function clientReportPDF() {

		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->clientReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'landscape' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream("OGMHC_Client_Report_" . Carbon::now()->toDateString());
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}


	}


}
