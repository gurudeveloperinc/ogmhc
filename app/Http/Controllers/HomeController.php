<?php

namespace App\Http\Controllers;

use App\client;
use App\faq;
use App\female;
use App\male;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }

	public  function login(Request $request){

		$email = $request->input('email');
		$password = $request->input('password');

		try {
			$user = client::where( 'email', $email )->where( 'password', $password )->first();
			$user->status = 1;
			return $user;
		} catch(\Exception $e){

			$response = array();
			$response['status'] = 0;

			return json_encode($response);

		}


	}

	public function getProcedures( Request $request ) {
		$category = $request->input('category');
		$type = $request->input('type');


		if($category == "male"){
			$procedures = male::where('type',$type)->get();
		}

		if($category == "female"){
			$procedures = female::where('type',$type)->get();
		}

		echo $procedures;

	}

	public function getFAQ() {
		$faq = faq::all();
		echo $faq;
	}

}
